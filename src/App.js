import React from 'react';
import './App.css';
import SimpleCard from './form';
import { ChakraProvider } from '@chakra-ui/react'


function App() {
  return (
    <ChakraProvider>
    <div className="App">
    <div style={{ backgroundImage: "url(/Anuvjain.jpeg)" }}>
      <SimpleCard></SimpleCard>
      </div>
    </div>
    </ChakraProvider>
  );
}

export default App;
