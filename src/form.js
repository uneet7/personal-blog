import React, { useState } from 'react';

import {
    Button,
    Flex,
    FormControl,
    FormLabel,
    Heading,
    Input,
    Stack,
    Avatar,
    AvatarBadge,
    IconButton,
    Center,
  } from '@chakra-ui/react';
  import mainLogo from'./Anuvjain.jpeg';

  export default function UserProfileEdit(): JSX.Element {
    const [count, setCount] = useState(0);

    const handleSubmit = () => {
        setCount(1);
    }

    if (count === 0){
        return (

        <form onSubmit={handleSubmit}>
          <Flex
            minH={'100vh'}
            align={'center'}
            justify={'center'}
        >
            <Stack
              spacing={4}
              w={'full'}
              maxW={'md'}
              rounded={'xl'}
              boxShadow={'lg'}
              p={6}
              my={12}>
              <Heading lineHeight={1.1} fontSize={{ base: '2xl', sm: '3xl' }}>
                Anuj Jain Live - Shott, Surat
              </Heading>
              <FormControl id="userName">
                <FormLabel>Entry Info</FormLabel>
                <Stack direction={['column', 'row']} spacing={6}>
                  <Center>
                    <Avatar size="xl" src={mainLogo}>
                      <AvatarBadge
                        as={IconButton}
                        size="sm"
                        rounded="full"
                        top="-10px"
                        colorScheme="red"
                        aria-label="remove Image"
                      />
                    </Avatar>
                  </Center>
                </Stack>
              </FormControl>
              <FormControl id="userName" isRequired>
                <FormLabel>Name</FormLabel>
                <Input
                  placeholder="UserName"
                  _placeholder={{ color: 'gray.500' }}
                  type="text"
                />
              </FormControl>
              <FormControl id="email" isRequired>
                <FormLabel>Email address</FormLabel>
                <Input
                  placeholder="your-email@example.com"
                  _placeholder={{ color: 'gray.500' }}
                  type="email"
                />
              </FormControl>
              <FormControl id="email" isRequired>
                <FormLabel>Phone number</FormLabel>
                <Input
                  placeholder="9xxxxxxxxx"
                  _placeholder={{ color: 'gray.500' }}
                  type="number"
                />
              </FormControl>
              <Stack spacing={6} direction={['column', 'row']}>
                <Button
                  bg={'blue.400'}
                  type="submit"
                  color={'white'}
                  w="full"
                  _hover={{
                    bg: 'blue.500',
                  }}>
                  Online Check-In
                </Button>
              </Stack>
            </Stack>
          </Flex>
          </form>    
    );
    }
    return (
        <Heading>You've successfully checked-in</Heading>
    )

  }